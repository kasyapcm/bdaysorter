import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class DateFileSorter {

	public static void main(String[] args) {
		File inputFile = new File("C:/Users/Chilakamarthy/Desktop/input.txt");
		File outputFile = new File("C:/Users/Chilakamarthy/Desktop/output.txt");
		Scanner sc = null;
		PrintWriter writer = null;
		List<CustomDate> dates = new LinkedList<CustomDate>();
		try {
			sc = new Scanner(inputFile);
			while (sc.hasNextLine()) {
				sc.nextInt();
				int month = sc.nextInt();
				int day = sc.nextInt();
				int year = sc.nextInt();
				CustomDate dateObj = new CustomDate(month, day, year);
				dates.add(dateObj);
			}

			Collections.sort(dates);

			writer = new PrintWriter(outputFile);

			int i = 1;
			for (CustomDate customDate : dates) {

				writer.print(i);
				writer.print(' ');
				writer.print(customDate.getMonth());
				writer.print(' ');

				writer.print(customDate.getDay());
				writer.print(' ');
				writer.println(customDate.getYear());
				i++;
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} finally {
			sc.close();
			writer.close();
		}

	}
}
