
/**
 * @author KAsH
 *
 *         CustomDate object represents a date. For a collection of CutomDate
 *         objects to be comparable they should implement the interface
 *         comparable thereby implement the compareTo method.
 */
public class CustomDate implements Comparable<CustomDate> {

	private int day;
	private int month;
	private int year;

	// parameterized constructor
	public CustomDate(int month, int day, int year) {
		this.month = month;
		this.day = day;
		this.year = year;
	}

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int compareTo(CustomDate o) {
		if (this.year > o.getYear())
			return 1;
		else if (this.year < o.getYear())
			return -1;
		else {
			if (this.month > o.getMonth())
				return 1 ;
			else if (this.month < o.getMonth())
				return -1;
			
			
			
			else {
				if (this.day > o.getDay())
					return 1;
				else if (this.day < o.getDay())
					return -1;
				else {
					return 0;
				}

			}

		}

	}

	
}
